function cacheFunction(cb) {
  let met = {};

  const  inner= ted=> {
    if (ted in met) {
return met[ted];
    }
    
    else {
      let temp = cb(ted);
      met[ted] = temp;
    //   console.log(met[ted])
    return true
    }
  }

  return inner;
}
module.exports = cacheFunction;
